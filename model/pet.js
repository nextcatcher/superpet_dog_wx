export class Pet {
  id;
  petNo;
  petName;
  birthday;
  avatar;
  sex;
  isNeuter;
  weight;
  feature;
  userid;
  userNickName;
  totalEnergy;
  constellation;
  breed;
  avatarTmp;

  constructor() {
    this.id = "";
    this.petNo = "";
    this.petName = "";
    this.birthday = "";
    this.avatar = "";
    this.sex = "-1";
    this.isNeuter = "-1";
    this.weight = "";
    this.feature = "";
    this.userid = "";
    this.userNickName = "";
    this.totalEnergy = "";
    this.constellation = "";
    this.breed = "";
    this.avatarTmp = "";
  }
}

export class PetPic {
  petPicId;
  petId;
  picPath;
  isDelete;//标记为删除，false-否，true-是

  constructor(petPicId, petId, picPath, isDelete) {
    this.petPicId = petPicId;
    this.petId = petId;
    this.picPath = picPath;
    this.isDelete = isDelete;
  }
}
